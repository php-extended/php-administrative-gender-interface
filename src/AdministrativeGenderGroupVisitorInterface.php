<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

use Stringable;

/**
 * AdministrativeGenderGroupVisitorInterface interface file.
 * 
 * This interface represents a visitor that visits administrative gender groups.
 * 
 * @author Anastaszor
 */
interface AdministrativeGenderGroupVisitorInterface extends Stringable
{
	
	/**
	 * Visits an empty group of administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitEmpty(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of one male only administrative gender.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitOneMale(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of multiple males only administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultipleMales(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of one female only administrative gender.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitOneFemale(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of multiple females only administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultipleFemales(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of one male and one female administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitOneMaleOneFemale(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of one male and multiple females administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitOneMaleMultipleFemales(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of multiple males and one female administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultipleMalesOneFemale(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of multiple males and multiple females administrative.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultipleMalesMultipleFemales(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of one unknown administrative gender.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitOneUnknown(AdministrativeGenderGroupInterface $group);
	
	/**
	 * Visits a group of multiple unknowns administrative genders.
	 * 
	 * @param AdministrativeGenderGroupInterface $group the group to test
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultipleUnknowns(AdministrativeGenderGroupInterface $group);
	
}
