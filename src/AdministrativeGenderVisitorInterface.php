<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

use Stringable;

/**
 * AdministrativeGenderVisitorInterface interface file.
 * 
 * This interface represents a visitor that visits administrative genders.
 * 
 * @author Anastaszor
 */
interface AdministrativeGenderVisitorInterface extends Stringable
{
	
	/**
	 * Visits a male administrative gender.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to visit
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMale(AdministrativeGenderInterface $gender);
	
	/**
	 * Visits a female administrative gender.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to visit
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFemale(AdministrativeGenderInterface $gender);
	
	/**
	 * Visits an unknown administrative gender.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to visit
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitUnknown(AdministrativeGenderInterface $gender);
	
}
