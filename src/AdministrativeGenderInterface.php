<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

/**
 * AdministrativeGenderInterface interface file.
 * 
 * This interface represents informations about a gender, for a given 
 * administrative definition of it.
 * 
 * @author Anastaszor
 */
interface AdministrativeGenderInterface
{
	
	/**
	 * Gets whether this gender represents a male character.
	 * 
	 * @return boolean
	 */
	public function isMale() : bool;
	
	/**
	 * Gets whether this gender represents a female character.
	 * 
	 * @return boolean
	 */
	public function isFemale() : bool;
	
	/**
	 * Visits this gender with given visitor.
	 * 
	 * @param AdministrativeGenderVisitorInterface $visitor the visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(AdministrativeGenderVisitorInterface $visitor);
	
	/**
	 * Merges this gender with another gender. Plural rules as administratively
	 * defined must be applied.
	 * 
	 * @param AdministrativeGenderInterface $other the other to merge with
	 * @return AdministrativeGenderInterface a new gender
	 */
	public function mergeWith(AdministrativeGenderInterface $other) : AdministrativeGenderInterface;
	
	/**
	 * Gets whether this gender equals this other gender.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
}
