<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

use Stringable;

/**
 * AdministrativeGenderGroupInterface interface file.
 * 
 * This interface represents informations about a gender for a group of given 
 * administrative definition of it.
 * 
 * @author Anastaszor
 */
interface AdministrativeGenderGroupInterface extends Stringable
{
	
	/**
	 * Gets whether this group is empty, i.e. contains no individuals.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this group contains the given gender.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to test against
	 * @return boolean
	 */
	public function contains(AdministrativeGenderInterface $gender) : bool;
	
	/**
	 * Gets whether this group contains multiple of the given gender.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to test against
	 * @return boolean
	 */
	public function containsMultiple(AdministrativeGenderInterface $gender) : bool;
	
	/**
	 * Absorbs the given gender into this group.
	 * 
	 * @param AdministrativeGenderInterface $gender the gender to absorb
	 * @return boolean
	 */
	public function absorb(AdministrativeGenderInterface $gender) : bool;
	
	/**
	 * Visits this gender with the given visitor.
	 * 
	 * @param AdministrativeGenderGroupVisitorInterface $visitor the visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(AdministrativeGenderGroupVisitorInterface $visitor);
	
	/**
	 * Merges this gender with another gender group. Plural rules as administratively
	 * defined must be applied.
	 * 
	 * @param AdministrativeGenderGroupInterface $other the group to merge with
	 * @return AdministrativeGenderGroupInterface
	 */
	public function mergeWith(AdministrativeGenderGroupInterface $other) : AdministrativeGenderGroupInterface;
	
	/**
	 * Gets whether this gender equals this other gender group.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
}
